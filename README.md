<!DOCTYPE html>
<html lang="en">

<body>
<div style="background-color: #f4511e;color: #ffffff;">

  <h1>Sentiment Analysis Techniques - A Comparative Study</h1>
  <h2>Master’s Thesis in Computer Science</h2>
  <h4>By Kamil Lipski</h4>
  <h3>COMPARING FOLLOWING ALGORITHMS:</h3>
</div>
<ol>
  <li>Naive Bayes</li>
  <li>Logistic Regression </li>
  <li>Support Vector Machines </li>
  <li>K-Nearest Neighbours</li>
  <li>Shallow Neural Network </li>
  <li>Deep Learning</li>
</ol>
</div>
<div>
<h3>DATASETS:</h3>
<ol>
  <li>UMICH SI650 - Sentiment Classification.
   ”UMICH SI650 - Sentiment Classification” - This dataset was retrieved from com-petition  on  Kaggle,  and  it  was  originally  hosted  by  University  of  Michigan  SI650(Information Retrieval).  The training data contains 7086 twitter sentences that arealready labeled with 1 (positive sentiment) or 0 (negative sentiment): https://www.kaggle.com/c/si650winter11/rules</li>

  <li> ”Twitter sentiment analysis - Sentiment Classification” - The dataset was retrievedfrom competition on Kaggle and taken from the real task of text processing.  Thetraining data contains 100k labeled twitter sentences labeled with 1 (positive senti-ment) or 0 (negative sentiment): https://www.kaggle.com/c/twitter-sentiment-analysis2/data. </li>

  <li>”Sentiment Analysis Dataset 2” - This text data was originally retrieved from Twitter with tool Sentiment140 for Sentiment Analysis3.  The training data contains 1.58million labeled 4 (positive) or 1 (negative sentences): https://www.kaggle.com/ywang311/twitter-sentiment</li>

</ol>
</div>

<div>
<h3>DATA TRANSFORMATION FEATURES</h3>
<ol>
    <li>Bag of Words</li>
    <li>TF-IDF</li>
</ol>
</div>

<div>
<h3>HYPERPARAMETER TUNING</h3>
<ol>
    <li>Of data transforming features</li>
    <li>Of Neural Networks</li>
</ol>
</div>

<div>
<h3>PLATFORMS USED IT THIS STUDY:</h3>
  <p>1. PYTHON 3.6.7, SCIKIT-LEARN, KERAS WITH TENSORFLOR AS BACKEND, PANDAS, NUMPY</p>
</div>
</body>
<div>
</div>
<footer>
Author Kamil Lipski
</footer>
</html>