# This class helps printing out data stored in csv files to latex table format

import pandas as pd
import pandasql as ps
from tabulate import tabulate

class Latex:
  def __init__(self, datasetpath):
    self.datasetpath = datasetpath

  def query(self, query):
    dataset = pd.read_csv(self.datasetpath)
    print(tabulate(ps.sqldf(query, locals()),headers='keys', showindex=False, tablefmt='latex'))

# HOW TO USE
# >>> l=Latex("results/accuracy_table/all_results_from_traditional_training.csv")
# >>> l.query("select * from dataset")
# \begin{tabular}{llrlr}
# \hline
#  dataset   & model\_name           &   accuracy & feature   &   test\_nr \\
# \hline
#  dataset\_1 & MultinomialNB        &   0.978805 & vect      &         1 \\
#  dataset\_1 & MultinomialNB        &   0.977842 & tfidf     &         1 \\
#  dataset\_1 & LogisticRegression   &   0.992775 & vect      &         1 \\
#  dataset\_1 & LogisticRegression   &   0.97158  & tfidf     &         1 \\
#  dataset\_1 & SGDClassifier        &   0.99422  & vect      &         1 \\
#  dataset\_1 & SGDClassifier        &   0.987476 & tfidf     &         1 \\
#  dataset\_1 & KNeighborsClassifier &   0.972543 & vect      &         1 \\
#  dataset\_1 & KNeighborsClassifier &   0.947013 & tfidf     &         1 \\
#  dataset\_2 & MultinomialNB        &   0.755109 & vect      &         2 \\
#  dataset\_2 & MultinomialNB        &   0.741274 & tfidf     &         2 \\
#  dataset\_2 & LogisticRegression   &   0.76561  & vect      &         2 \\
#  dataset\_2 & LogisticRegression   &   0.771477 & tfidf     &         2 \\
#  dataset\_2 & SGDClassifier        &   0.767677 & vect      &         2 \\
#  dataset\_2 & SGDClassifier        &   0.758376 & tfidf     &         2 \\
#  dataset\_2 & KNeighborsClassifier &   0.6643   & vect      &         2 \\
#  dataset\_2 & KNeighborsClassifier &   0.680001 & tfidf     &         2 \\
#  dataset\_3 & MultinomialNB        &   0.779678 & vect      &         3 \\
#  dataset\_3 & MultinomialNB        &   0.771876 & tfidf     &         3 \\
#  dataset\_3 & LogisticRegression   &   0.798414 & vect      &         3 \\
#  dataset\_3 & LogisticRegression   &   0.801334 & tfidf     &         3 \\
#  dataset\_3 & SGDClassifier        &   0.789378 & vect      &         3 \\
#  dataset\_3 & SGDClassifier        &   0.775358 & tfidf     &         3 \\
#  dataset\_3 & KNeighborsClassifier &   0.703446 & vect      &         3 \\
#  dataset\_3 & KNeighborsClassifier &   0.704061 & tfidf     &         3 \\
# \hline
# \end{tabular}