# main.py
# SENTIMENT ANALYSIS - MAIN
# AUTHOR: KAMIL LIPSKI
# YEAR: 2019 - 2021

# RESOURCES:
# https://machinelearningmastery.com/machine-learning-in-python-step-by-step/
# https://machinelearningmastery.com/prepare-movie-review-data-sentiment-analysis/
# https://scikit-learn.org/stable/tutorial/text_analytics/working_with_text_data.html
# http://cmdlinetips.com/2018/11/string-manipulations-in-pandas/
# w3schools.com/python
# https://www.analyticsvidhya.com/blog/2017/06/word-embeddings-count-word2veec/

# DESCRIPTION:
# Main file in this project
# Executing this file will start all the tests
# It additionally runs exploration of datasets

import pandas as pd
from models.training import Train
from models.data_exploring import ExploringData
import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning)

# >>> IMPORTING DATA >>>
train_1 = pd.read_csv("datasets/dataset_1/train.csv", header='infer', index_col=None)
train_2 = pd.read_csv("datasets/dataset_2/train.csv", header='infer',index_col=None)
train_3 = pd.read_csv("datasets/dataset_3/train.csv", delimiter=None, header='infer', names=None,  encoding='latin-1', error_bad_lines=False)
# <<< IMPORTING DATA <<<

# >>> EXPLORING DATA >>>
ExploringData(train_1,"dataset_1","SentimentText","Sentiment").runall()
ExploringData(train_2,"dataset_2","SentimentText","Sentiment").runall()
ExploringData(train_3,"dataset_3","SentimentText","Sentiment").runall()
ExploringData(train_4,"dataset_4","SentimentText","Sentiment").runall()
# <<< EXPLORING DATA <<<

# >>> PERFOMRING TESTS >>>
Train(train = train_1, datasetname = "dataset_1", train_tweet = "SentimentText", train_label = "Sentiment", cleantweet=True, deep_learning_neural_networks=True)
Train(train_2,'dataset_2',"SentimentText","Sentiment")
Train(train_3,'dataset_3',"SentimentText","Sentiment")
# <<< PERFORMING TESTS <<<

exit(0)