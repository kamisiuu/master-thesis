import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.naive_bayes import MultinomialNB
from sklearn.pipeline import Pipeline

from models.SVCgridSearch import paramTunSVC

import time
from warnings import filterwarnings
filterwarnings('ignore')
import pandas as pd
from sklearn import metrics
from sklearn.ensemble import RandomForestClassifier
from sklearn.feature_extraction import FeatureHasher, stop_words
# Note the problem is too easy: the hyperparameter plateau is too flat and the
# output model is the same for precision and recall with ties in quality.
from sklearn.feature_extraction.text import (CountVectorizer,
                                             HashingVectorizer,
                                             TfidfTransformer, TfidfVectorizer)
from sklearn.linear_model import LogisticRegression, SGDClassifier
from sklearn.metrics import classification_report, f1_score
from sklearn.model_selection import (GridSearchCV, cross_val_score,
                                     train_test_split)
from sklearn.naive_bayes import MultinomialNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.pipeline import Pipeline
from xgboost import XGBClassifier
import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning)

from models.tweet_cleaner import tweet_cleaner
import pandas as pd
import pandasql as ps
# from io import StringIO
from tabulate import tabulate

import csv

dataset=[]
with open("datasets/dataset_4/train.tsv") as tsvfile:
    reader = csv.reader(tsvfile, delimiter="\t")
    for row in reader:
        dataset.append(row)
data = pd.DataFrame(dataset)

data.to_csv('datasets/dataset_4/train.csv',
               mode='a', index_label=False, header=False, index=False)

train_4 = pd.read_csv("datasets/dataset_4/train.csv", delimiter=None, header='infer', names=None,  encoding='latin-1')
train_clean = train_4[['Sentiment','Phrase',]]
train_clean.to_csv('datasets/dataset_4/train2.csv',
               mode='a', index_label=False, header=True, index=False)
print(train_clean)
# #
# tfidf_all_results= pd.read_csv(
#     "results/accuracy_table/gridsearch_features/tfidf/all_results_from_feature_gridsearch.csv")
# # dataset,model_name,vect__analyzer,vect__ngram_range,vect__lower_case,vect__max_df,accuracy
# q1 = """ select * from tfidf_all_results where model_name='MultinomialNB' and dataset="dataset_1"
# order by accuracy desc  """
# # print(tabulate(ps.sqldf(q1,locals()), headers='keys', tablefmt='psql'))
#
# #print(ps.sqldf(q2,locals()))
#
# # # print()
# #
# train = pd.read_csv("datasets/dataset_2/train.csv", header='infer', index_col=None)
# # train=tweet_cleaner(train,'SentimentText',preprocessoptions=['stemming','tokenization'])
# # print(train)
# xtrain,xvalid,ytrain,yvalid = train_test_split(train['SentimentText'],train['Sentiment'],test_size=0.3)
#
#
# class BestFeature:
#     def __new__(cls, name, feature, train, x_train, x_valid):
#         cls.name = name
#         cls.train = train
#         cls.feature = feature
#         cls.x_train = x_train
#         cls.x_valid = x_valid
#
#         # transforming earlier splitted data into feature
#         feature_vect = feature
#         feature_vect.fit(train)
#         xtrain_feature = feature_vect.transform(x_train)
#         xvalid_feature = feature_vect.transform(x_valid)
#         # feature returns tranformed
#         return xtrain_feature, xvalid_feature
#
#
# class paramCountVectSearch:
#     def __new__(cls, datasetname, classifier, xtrain, ytrain):
#         print("Running method:",cls.__name__,", dataset: ",datasetname,"\nStarting parameter optimization for "
#                                                                        "feature CountVectorizer() "
#                                              "with classifier ",classifier.__class__.__name__ , )
#         start = time.time()
#         cls.datasetname=datasetname
#         cls.ytrain= ytrain
#         cls.classfier = classifier
#         cls.xtrain = xtrain
#
#         clf_name=str(classifier.__class__.__name__)
#         all_results = pd.read_csv(
#             "results/accuracy_table/gridsearch_features/vect/all_results_from_feature_gridsearch.csv")
#         query = """ select test_nr from all_results where test_nr is not Null order by test_nr desc limit 1 """
#         df = ps.sqldf(query, locals())
#         new_test_nr = 0
#         if (df.empty == True):
#             new_test_nr = 1
#         else:
#             lasttestnr = df['test_nr'][df.index[-1]]
#             new_test_nr = lasttestnr + 1
#
#         pipeline = Pipeline([
#             ('vect',CountVectorizer()),
#             ('clf',classifier)
#         ])
#
#         param_grid = {
#             'vect__max_df': (0.5, 0.75, 1.0),
#             'vect__lowercase': [True, False],
#             'vect__ngram_range': [(1, 1), (1, 2), (1, 3)],
#             'vect__analyzer': ['word','char','char_wb']
#         }
#
#         grid = GridSearchCV(pipeline,param_grid,cv=5)
#         grid_result = grid.fit(cls.xtrain, cls.ytrain)
#
#         # summarize results
#         best_result = []
#         best_params = grid_result.best_params_
#         best_score = grid_result.best_score_
#         best_result.append((datasetname,classifier.__class__.__name__,best_params['vect__analyzer'],
#                             best_params['vect__ngram_range'],best_params['vect__lowercase'],
#                             best_params['vect__max_df'],best_score,new_test_nr))
#         cv_best = pd.DataFrame(best_result, columns=['dataset','model_name','vect__analyzer','vect__ngram_range',
#                                                     'vect__lower_case','vect__max_df','accuracy','test_nr'])
#         cv_best.to_csv('results/accuracy_table/gridsearch_features/vect/best_results_from_feature_gridsearch.csv',
#                mode='a', index_label=False, header=False, index=False)
#         means = grid_result.cv_results_['mean_test_score']
#         stds = grid_result.cv_results_['std_test_score']
#         params = grid_result.cv_results_['params']
#
#         all_results=[]
#         for mean, stdev, param in zip(means, stds, params):
#             all_results.append((datasetname,classifier.__class__.__name__, param['vect__analyzer'],
#                                 param['vect__ngram_range'], param['vect__lowercase'], param['vect__max_df'],
#                                 mean,new_test_nr))
#             #print("%f (%f) with: %r" % (mean, stdev, param))
#
#         cv_all = pd.DataFrame(all_results, columns=['dataset','model_name','vect__analyzer','vect__ngram_range',
#                                                     'vect__lower_case','vect__max_df','accuracy','test_nr'])
#         cv_all.to_csv('results/accuracy_table/gridsearch_features/vect/all_results_from_feature_gridsearch.csv',
#                       mode='a', index_label=False, header=False,index=False)
#         end = time.time()
#         result = (end-start) / 60
#
#         newest_all_results = pd.read_csv(
#             "results/accuracy_table/gridsearch_features/vect/all_results_from_feature_gridsearch.csv")
#
#         q2 = "select * from newest_all_results where model_name='"+clf_name+"' and test_nr='"+str(new_test_nr)+\
#              "' order by accuracy desc"
#
#         print(tabulate(ps.sqldf(q2, locals()),headers='keys',tablefmt='psql'))
#         print("Best: %f using %s" % (grid_result.best_score_, grid_result.best_params_))
#         print("Done running method: ", cls.__name__, ", dataset: ", datasetname,
#               "\nTime spent on parameter optimization for feature CountVectorizer() with classifier ",
#               classifier.__class__.__name__, " and dataset ", datasetname, ": ", "%.2f" % result, " minutes\n")
#         return grid_result.best_params_
#
#
# class paramTfidfSearch:
#     import matplotlib.pyplot as plt
#     import numpy as np
#     # def plot_grid_search(cv_results, grid_param_1, grid_param_2, name_param_1, name_param_2):
#     #     # Get Test Scores Mean and std for each grid search
#     #     scores_mean = cv_results['mean_test_score']
#     #     scores_mean = np.array(scores_mean).reshape(len(grid_param_2),len(grid_param_1))
#     #
#     #     scores_sd = cv_results['std_test_score']
#     #     scores_sd = np.array(scores_sd).reshape(len(grid_param_2),len(grid_param_1))
#     #
#     #     # Plot Grid search scores
#     #     _, ax = plt.subplots(1,1)
#     #
#     #     # Param1 is the X-axis, Param 2 is represented as a different curve (color line)
#     #     for idx, val in enumerate(grid_param_2):
#     #         ax.plot(grid_param_1, scores_mean[idx,:], '-o', label= name_param_2 + ': ' + str(val))
#     #
#     #     ax.set_title("Grid Search Scores", fontsize=20, fontweight='bold')
#     #     ax.set_xlabel(name_param_1, fontsize=16)
#     #     ax.set_ylabel('CV Average Score', fontsize=16)
#     #     ax.legend(loc="best", fontsize=15)
#     #     ax.grid('on')
#
#     def __new__(cls, datasetname, classifier, xtrain, ytrain):

#         import matplotlib.pyplot as plt
#         print("Running method:",cls.__name__,", dataset: ",datasetname,
#               "\nStarting parameter optimization for feature TfidfVectorizer() "
#                                              "with classifier ",classifier.__class__.__name__ , )
#         start = time.time()
#         cls.datasetname=datasetname
#         cls.ytrain= ytrain
#         cls.classfier = classifier
#         cls.xtrain = xtrain
#
#         clf_name = str(classifier.__class__.__name__)
#         all_results = pd.read_csv(
#             "results/accuracy_table/gridsearch_features/tfidf/all_results_from_feature_gridsearch.csv")
#         query = """ select test_nr from all_results where test_nr is not Null order by test_nr desc limit 1 """
#         df = ps.sqldf(query, locals())
#         new_test_nr = 0
#         if (df.empty == True):
#             new_test_nr = 1
#         else:
#             lasttestnr = df['test_nr'][df.index[-1]]
#             new_test_nr = lasttestnr + 1
#
#         pipeline = Pipeline([
#             ('tfidf',TfidfVectorizer(sublinear_tf=True)),
#             ('clf',classifier)
#         ])
#
#         param_grid = {
#             'tfidf__use_idf': [True,False],
#             'tfidf__lowercase': [True, False],
#             'tfidf__ngram_range': [(1, 1), (1, 2), (1, 3)],
#             'tfidf__analyzer': ['word','char','char_wb']
#         }
#
#         grid = GridSearchCV(pipeline,param_grid,cv=5)
#         grid_result = grid.fit(cls.xtrain, cls.ytrain)
#
#         # summarize results
#         best_result = []
#         best_params = grid_result.best_params_
#         best_score = grid_result.best_score_
#         best_result.append((datasetname,classifier.__class__.__name__,best_params['tfidf__analyzer'],
#                             best_params['tfidf__ngram_range'],best_params['tfidf__lowercase'],
#                             best_params['tfidf__use_idf'],best_score,new_test_nr))
#         cv_best = pd.DataFrame(best_result, columns=['dataset','model_name','tfidf__analyzer','tfidf__ngram_range',
#                                                     'tfidf__lower_case','tfidf__use_idf','accuracy','test_nr'])
#         cv_best.to_csv('results/accuracy_table/gridsearch_features/tfidf/best_results_from_feature_gridsearch.csv',
#                mode='a', index_label=False, header=False, index=False)
#         means = grid_result.cv_results_['mean_test_score']
#         stds = grid_result.cv_results_['std_test_score']
#         params = grid_result.cv_results_['params']
#
#         all_results=[]
#         for mean, stdev, param in zip(means, stds, params):
#             all_results.append((datasetname,classifier.__class__.__name__, param['tfidf__analyzer'],
#                                 param['tfidf__ngram_range'], param['tfidf__lowercase'], param['tfidf__use_idf'],
#                                 mean,new_test_nr))
#             #print("%f (%f) with: %r" % (mean, stdev, param))
#
#         cv_all = pd.DataFrame(all_results, columns=['dataset','model_name','tfidf__analyzer','tfidf__ngram_range',
#                                                     'tfidf__lower_case','tfidf__use_idf','accuracy','test_nr'])
#         cv_all.to_csv('results/accuracy_table/gridsearch_features/tfidf/all_results_from_feature_gridsearch.csv',
#                       mode='a', index_label=False, header=False,index=False)
#         end = time.time()
#         result = (end-start) / 60
#
#         newest_all_results = pd.read_csv(
#             "results/accuracy_table/gridsearch_features/tfidf/all_results_from_feature_gridsearch.csv")
#
#         q2 = "select * from newest_all_results where model_name='" + clf_name + "' and test_nr='" + str(new_test_nr) + \
#              "' order by accuracy desc"
#
#         print(tabulate(ps.sqldf(q2, locals()), headers='keys', tablefmt='psql'))
#
#         print("Best: %f using %s" % (grid_result.best_score_, grid_result.best_params_))
#         print("Done running method: ",cls.__name__,", dataset: ",datasetname,
#               "\nTime spent on parameter optimization for feature TfidfVectorizer() with classifier ",
#               classifier.__class__.__name__, " and dataset ",datasetname, ": ", "%.2f" % result ," minutes\n")
#         return grid_result.best_params_
#
# # paramTfidfSearch("dataset1",LogisticRegression(),xtrain,ytrain)
# # models = [LogisticRegression()]
#
#
# #
# # for model in models:
# #     paramCountVectSearch("dataset1",model,xtrain,ytrain)
# # #
# # # # classifier = SGDClassifier()
# # # # classifier.fit(xtrain_count,ytrain)
# # # #
# # # # prediction=classifier.predict(xvalid_count)
# # # # #accuracy = classification_report(prediction,yvalid)
# # # # accuracy = metrics.accuracy_score(prediction,yvalid)
# # # #
# # # # print(accuracy)
# # # # entries=[]
# # # # accuracies = cross_val_score(classifier, xtrain_count, ytrain, scoring='accuracy', cv=100)
# # # # print(accuracies)
# # # # for fold_idx, accuracy in enumerate(accuracies):
# # # #     entries.append((classifier.__class__.__name__, fold_idx, accuracy))
# # # # cv_df = pd.DataFrame(entries, columns=['model_name', 'fold_idx', 'accuracy'])
# # # # print(cv_df.groupby('model_name').accuracy.mean())
# # # #
# # #
# # # # #test = pd.read_csv("data/dataset_1/test.csv", header='infer', index_col=None)
# # # #
# # # # from classes.tweet_cleaner import tweet_cleaner
# # # # #data = pd.read_csv('data/results/accuracy_table/all_results_from_training.csv')
# # #
# # #
# # # # # count_vect = CountVectorizer(analyzer='word', token_pattern=r'\w{1,}')
# # # # # xtrain_count =count_vect.fit_transform(train['SentimentText'])
# # # # #
# # # # # paramTunSVC(xtrain_count,train['Sentiment'])
# # # #
# # # #
# # # #
# # #
# # #
# # #
# # #
# # # # START TRAINING WITH DEEP NEURAL NETWORKS+
# # # # classifierList = {'CNN': 'create_cnn()', 'RCNN': 'create_rcnn()', 'RNN-LSTM': 'create_rnn_lstm()',
# # # #                   'RNN-GRU': 'create_rnn_gru()', 'BIDIRECTIONAL-RNN': 'create_bidirectional_rnn()'}
# # # #
# # # # for choicemodel in classifierList:
# # # #     #print (choicemodel)
# # # #     print( classifierList[choicemodel])
# # # # from classes.grid_search_utility import grid_search_svm
# # # # import gensim
# # # # from gensim.models import Word2Vec
# # # #
# # # # #loading the downloaded model
# # # # model = gensim.models.KeyedVectors.load_word2vec_format('data/GoogleNews-vectors-negative300.bin', binary=True)
# # # #
# # # # #the model is loaded. It can be used to perform all of the tasks mentioned above.
# # # #
# # # # # getting word vectors of a word
# # # # dog = model['dog']
# # # #
# # # # #performing king queen magic
# # # # print(model.most_similar(positive=['woman', 'king'], negative=['man']))
# # # #
# # # # #picking odd one out
# # # # print(model.doesnt_match("breakfast cereal dinner lunch".split()))
# # # #
# # # # #printing similarity index
# # # # print(model.similarity('woman', 'man'))
# # #
# # #
# # # # text_clf = Pipeline([('vect', CountVectorizer()),
# # # #                      ('tfidf', TfidfTransformer()),
# # # #                      ('clf', MultinomialNB())])
# # # #reatly simplifies JavaScript programming.
# # #
# # #
# # # # tuned_parameters = {
# # # #     'vect__ngram_range': [(1, 1), (1, 2), (2, 2)],
# # # #     'tfidf__use_idf': (True, False),
# # # #     'tfidf__norm': ('l1', 'l2'),
# # # #     'clf__alpha': [1, 1e-1, 1e-2]
# # # # }
# # # #
# # # #
# # # # x_train, x_test, y_train, y_test = train_test_split(train['tweet'], train['label'], random_state=42,
# # # #                                                test_size=0.3)
# # # #
# # # #
# # # # from sklearn.metrics import classification_report
# # # # clf = GridSearchCV(text_clf, tuned_parameters, cv=10)
# # # # clf.fit(x_train, y_train)
# # # #
# # # # print(classification_report(y_test, clf.predict(x_test), digits=4))
# # # #
# # # #
# # # #
# # # # from sklearn import datasets
# # # # from sklearn.model_selection import train_test_split
# # # # from sklearn.model_selection import GridSearchCV
# # # # from sklearn.metrics import classification_report
# # # # from sklearn.svm import SVC
# # # #
# # # # print(__doc__)
# # # #
# # # # # Loading the Digits dataset
# # # # digits = datasets.load_digits()
# # # #
# # # # # To apply an classifier on this data, we need to flatten the image, to
# # # # # turn the data in a (samples, feature) matrix:
# # # # n_samples = len(digits.images)
# # # # X = digits.images.reshape((n_samples, -1))
# # # # y = digits.target
# # # #
# # # # # Split the dataset in two equal parts
# # # # X_train, X_test, y_train, y_test = train_test_split(
# # # #     X, y, test_size=0.5, random_state=0)
# # # #
# # # # # Set the parameters by cross-validation
# # # # tuned_parameters = [{'kernel': ['rbf'], 'gamma': [1e-3, 1e-4],
# # # #                      'C': [1, 10, 100, 1000]},
# # # #                     {'kernel': ['linear'], 'C': [1, 10, 100, 1000]}]
# # # #
# # # # scores = ['precision', 'recall']
# # # #
# # # # for score in scores:
# # # #     print("# Tuning hyper-parameters for %s" % score)
# # # #     print()
# # # #
# # # #     clf = GridSearchCV(SVC(), tuned_parameters, cv=5,
# # # #                        scoring='%s_macro' % score)
# # # #     clf.fit(X_train, y_train)
# # # #
# # # #     print("Best parameters set found on development set:")
# # # #     print()
# # # #     print(clf.best_params_)
# # # #     print()
# # # #     print("Grid scores on development set:")
# # # #     print()
# # # #     means = clf.cv_results_['mean_test_score']
# # # #     stds = clf.cv_results_['std_test_score']
# # # #     for mean, std, params in zip(means, stds, clf.cv_results_['params']):
# # # #         print("%0.3f (+/-%0.03f) for %r"
# # # #               % (mean, std * 2, params))
# # # #     print()
# # # #
# # # #     print("Detailed classification report:")
# # # #     print()
# # # #     print("The model is trained on the full development set.")
# # # #     print("The scores are computed on the full evaluation set.")
# # # #     print()
# # # #     y_true, y_pred = y_test, clf.predict(X_test)
# # # #     print(classification_report(y_true, y_pred))
# # # #     print()
