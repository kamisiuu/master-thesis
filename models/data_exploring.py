# data_exploring.py
# SENTIMENT ANALYSIS - DATA EXPLORING
# AUTHOR: KAMIL LIPSKI
# YEAR: 2019 - 2021

# RESOURCES:
# https://www.analyticsvidhya.com/blog/2018/07/hands-on-sentiment-analysis-dataset-python/
# https://realpython.com/documenting-python-code/
# https://www.python-graph-gallery.com/

# DESCRIPTION:
# This will help you to perform some basic exploring, like counting categories within columns,
# check for missing values
# you can additionally explore common words, racist tweets, sexiest words and most used hashtags

from threading import Thread

import pandas as pd
import seaborn as sns
import nltk

import matplotlib.pyplot as plt
import re

from wordcloud import WordCloud


class ExploringData:
    """ This is a class ExploringData

    Example
    -------
    df = ExploringData(data,datasetname,data_text,data_label).runall()

    Attributes
    ----------
    data : dataFrame
        a full dataset
    datasetname : str
        the name of the dataset for example: "dataset_1"
    data_text : str
        a name of column that contains text data
    data_label : str
        a name of a column that contains

    Methods
    -------
    count_categories_within_column(self,plot=False)
        This function counts number of categories within each column
    run_all()

    Returns
    -------
    it returns dataframe


    """
    def __init__(self,data,datasetname,data_text,data_label):
        self.data=data
        self.datasetname=datasetname
        self.data_text=data_text
        self.data_label=data_label

    def count_categories_within_column(self,plot=False):
        """ This function counts number of categories within each column

        If the argument `plot` isn't passed in, the default is plot is False which means diagrams won't be plotted

        Parameters
        ----------
        plot : bool, optional

        """
        print("\n")
        print("Performing data exploring on : "+self.datasetname)
        print("Total number of tweets: "+ str(len(self.data.axes[0]))+ ", shape of the dataset: ", self.data.shape)
        # print('\nNumber of each category within column\n',
        #       self.data[self.data_label].value_counts())
        print("Categories and total number of each:\n", self.data.groupby('Sentiment')['SentimentText'].agg(['count']))
        if plot:
            plot_size = plt.rcParams["figure.figsize"]
            plot_size[0] = 8
            plot_size[1] = 6
            plt.rcParams["figure.figsize"] = plot_size

            self.data[self.data_label].\
                value_counts().plot(kind='pie', autopct='%1.0f%%', shadow=True,
                                    colors=["yellow","red","blue","green", "orange"],
                                    title="Percentage diagram of sentiment categories number of each category for: "
                                          ""+self.datasetname)

    # This function counts number of missing values within whole dataset
    def checkfor_missingvalues(self):
        print ('\nMissing Values in dataset\n',self.data.isnull().sum())

    # This function shows a diagram with common words used in
    # the tweets: WordCloud
    def explore_common_words(self):
        all_words=' '.join([text for text in self.data[self.data_text]])

        wordcloud=WordCloud(width=800, height=500, random_state=21,
                        max_font_size=110).generate(all_words)

        plt.figure(figsize=(10, 7))
        plt.imshow(wordcloud, interpolation="bilinear")
        plt.title('Common words for: '+self.datasetname)
        plt.axis('off')
        plt.show()

    # This function plots diagram with words in non racist/sexist tweets
    def explore_non_racist_sexist_tweets(self):
        normal_words = ' '.join([text for text in self.data[self.data_text]
        [self.data[self.data_label] == 0]])
        wordcloud = WordCloud(width=800, height=500, random_state=21,
                              max_font_size=110).generate(normal_words)
        plt.figure(figsize=(10, 7))
        plt.imshow(wordcloud, interpolation="bilinear")
        plt.axis('off')
        plt.title('Words in non racist/sexist tweets for: '+self.datasetname)
        plt.show()

    # This function plots diagram with racist/Sexist Tweets
    def explore_racist_sexist_tweets(self):
        negative_words = ' '.join([text for text in
                                   self.data[self.data_text]
        [self.data[self.data_label] == 1]])
        wordcloud = WordCloud(width=800, height=500,
                          random_state=21, max_font_size=110).\
            generate(negative_words)
        plt.figure(figsize=(10, 7))
        plt.imshow(wordcloud, interpolation="bilinear")
        plt.axis('off')
        plt.title('Words in racist/sexist tweets for: ' + self.datasetname)
        plt.show()

    # Understanding the impact of Hashtags on tweets sentiment
    def explore_hashtags(self):
        def hashtag_extract(x):
            hashtags = []
            # Loop over the words in the tweet
            for i in x:
                ht = re.findall(r"#(\w+)", i)
                hashtags.append(ht)
            return hashtags
        try:
            # extracting hashtags from non racist/sexist tweets
            HT_regular = hashtag_extract(
                self.data[self.data_text][self.data[self.data_label]==0])
            # extracting hashtags from racist/sexist tweets
            HT_negative = hashtag_extract(
                self.data[self.data_text][self.data[self.data_label]==1])

            # unnesting list
            HT_regular = sum(HT_regular, [])
            HT_negative = sum(HT_negative, [])
            a = nltk.FreqDist(HT_regular)
            d = pd.DataFrame({'Hashtag': list(a.keys()),
                              'Count': list(a.values())})
            # selecting top 10 most frequent hashtags
            d = d.nlargest(columns="Count", n=10)
            plt.figure(figsize=(16, 5))
            ax = sns.barplot(data=d, x="Hashtag", y="Count")
            ax.set(ylabel='Count')
            plt.title('Non-Racist/Sexist Tweets of HASHTAGS')
            plt.show()

            b = nltk.FreqDist(HT_negative)
            e = pd.DataFrame({'Hashtag': list(b.keys()),'Count':
                list(b.values())})
            # selecting top 10 most frequent hashtags
            e = e.nlargest(columns="Count", n=10)
            plt.figure(figsize=(16, 5))
            ax = sns.barplot(data=e, x="Hashtag", y="Count")
            ax.set(ylabel='Count')
            plt.title('Racist/Sexist Tweets of HASHTAGS')
            plt.show()
        except Exception:
            print ("\nThere are no hashtags in your data therefor two plots will not be shown")
            pass
        else:pass


    # this method runs all previous methods
    def runall(self):
        self.count_categories_within_column(plot=True)
        self.checkfor_missingvalues()
        self.explore_common_words()
        self.explore_non_racist_sexist_tweets()
        self.explore_racist_sexist_tweets()
        self.explore_hashtags()

