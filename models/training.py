# training.py
# SENTIMENT ANALYSIS - TRAINING
# AUTHOR: KAMIL LIPSKI
# YEAR: 2019 - 2021

# RESOURCES:
# https://towardsdatascience.com/building-a-deep-learning-model-using-keras-1548ca149d37
# https://www.kaggle.com/kanncaa1/deep-learning-tutorial-for-beginners

# DESCRIPTION:
# This class enables defined methods from class grid_search_utility.py
# and stores all the results from tests

# >>> IMPORTS >>>
import pandas as pd
import numpy as np
import pandasql as ps
from joblib import Parallel, delayed
from keras.optimizers import SGD
from keras.wrappers.scikit_learn import KerasClassifier
from keras_preprocessing.sequence import pad_sequences
from keras_preprocessing.text import Tokenizer
from keras.optimizers import Nadam
from keras.constraints import maxnorm
from sklearn.ensemble import RandomForestClassifier
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer, HashingVectorizer
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.naive_bayes import MultinomialNB
from sklearn.linear_model import LogisticRegression, SGDClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn import metrics, preprocessing
from tensorflow import keras
from keras.layers import Embedding, LSTM, Dense
from keras import models as mlp
from models.data_exploring import ExploringData
from models import tweet_cleaner as dataclean
from models.grid_search_utility import *
from sklearn.preprocessing import LabelEncoder
from keras.models import Sequential
from keras.layers import Dense, Activation, Dropout
from keras.preprocessing import text
from keras import utils
from keras.models import Sequential
from models.grid_search_utility import grid_search_neural_network
from keras import layers, optimizers, Model
import time
import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning)
#<<< IMPORTS <<<

def Train(train, datasetname, train_tweet, train_label, dataexplore = False, cleantweet = False, traditional_ml = False, param_s_count_vect = False, params_s_tfidf = False, shallow_neural_networks = False, neural_grid_seach = False, deep_learning_neural_networks = False):
    start = time.time()

    if dataexplore:
        exp1= ExploringData(train,datasetname,train_tweet,train_label)
        exp1.runall()
    if cleantweet:
        train = dataclean.tweet_cleaner(train, train_tweet,preprocessoptions=['noise', 'short_words', 'stop_words', 'rare_words', 'common_words', 'tokenization', 'lower_case'])

    # >>> PREPROCESSING START >>>
    # splitting data into training and validation set
    x_train, x_test, y_train, y_test = train_test_split(train[train_tweet], train[train_label], random_state=1000, train_size=0.7, test_size=0.3)

    # >>> COUNT VECTORIZER >>>
    count_vect = CountVectorizer()
    count_vect.fit(train[train_tweet])
    xtrain_count = count_vect.transform(x_train)
    xvalid_count = count_vect.transform(x_test)
    # <<< COUNT VECTORIZER <<<

    # >>> TF-IDF WORD LEVEL >>>
    tfidf_vect = TfidfVectorizer()
    tfidf_vect.fit(train[train_tweet])
    xtrain_tfidf = tfidf_vect.transform(x_train)
    xvalid_tfidf = tfidf_vect.transform(x_test)
    # <<< TF-IDF WORD LEVEL <<<

    # This class is made to keep features in a list, and make it easier to use it in loops
    class Feature:
        def __init__(self, name, xtrain=[], xvalid=[]):
            self.name = name
            self.xtrain = xtrain
            self.xvalid = xvalid

    featureList = []
    featureList.append(Feature('vect', xtrain_count, xvalid_count))
    featureList.append(Feature('tfidf', xtrain_tfidf, xvalid_tfidf))
    # <<< PREPROCESSING END <<<

    def train_model(classifier, model_name, feature_name, feature_vector_train, label, feature_vector_valid, batch_size=False):
        model = classifier
        model.fit(feature_vector_train, label)
        all_results = pd.read_csv("results/accuracy_table/all_results_from_traditional_training.csv")
        query = """ select test_nr from all_results where test_nr is not Null order by test_nr desc limit 1 """
        df = ps.sqldf(query, locals())
        new_test_nr = 0
        if (df.empty == True):
            new_test_nr = 1
        else:
            lasttestnr = df['test_nr'][df.index[-1]]
            new_test_nr = lasttestnr + 1

        # predict the labels on validation dataset
        predictions = model.predict(feature_vector_valid)
        entries.append((datasetname, model_name, metrics.accuracy_score(predictions, y_test),
                        feature_name, new_test_nr))
        return metrics.accuracy_score(predictions, y_test)

    def shallow_neural_network(batch_size, epochs, dropout, optimizer, loss, feature_vector_train, feature_vector_valid, feature_name):
        model = Sequential()
        input_dim= feature_vector_train.shape[1]
        model.add(Dense(512, input_dim=input_dim))
        model.add(Dropout(dropout))
        model.add(Dense(num_classes))
        model.add(Activation('softmax'))
        model.compile(loss=loss,optimizer=optimizer, metrics=['accuracy'])
        history = model.fit(feature_vector_train, y_train, batch_size=batch_size, epochs=epochs, verbose=1)
        score = model.evaluate(feature_vector_valid, y_test, batch_size=batch_size, verbose=1)
        storeresults.append((datasetname, "Shallow Neural Networks",score[1], feature_name, epochs, batch_size, dropout,optimizer,loss))
        print('Test accuracy:', score[1])
        return model

    def deep_learning_neural_network(hiddenlayers, batch_size, epochs, dropout, optimizer, init_mode, activation, weight_constraint, neurons, feature_vector_train, feature_vector_valid, feature_name):
        model = Sequential()
        input_dim = feature_vector_train.shape[1]
        model.add(Dense(neurons, input_dim=input_dim, kernel_initializer=init_mode, activation=activation, kernel_constraint=maxnorm(weight_constraint)))
        model.add(Dropout(dropout))
        for hiddenlayer in range(hiddenlayers):
            print("adding hidden layer: ", hiddenlayer+1)
            model.add(Dense(neurons))
            model.add(Dropout(dropout))
        model.add(Dense(num_classes))
        model.add(Activation('softmax'))
        model.compile(loss="binary_crossentropy", optimizer=optimizer, metrics=['accuracy'])
        history = model.fit(feature_vector_train, y_train,batch_size=batch_size,epochs=epochs,verbose=1)
        score = model.evaluate(feature_vector_valid, y_test,batch_size=batch_size, verbose=1)
        final_deep_learnresults.append((datasetname, "Deep Neural Network", score[1], feature_name, hiddenlayers, epochs,batch_size, dropout, optimizer))
        print('Test accuracy:', score[1])
        return model

    # >>> CLASSIFIERS >>>
    entries = []
    # models = [MultinomialNB(), LogisticRegression(solver='liblinear'), SGDClassifier(shuffle=True)]
    models = [LogisticRegression(solver='sag', max_iter=120000)]
    # <<< CLASSIFIERS <<<

    # # >>> TRADITIONAL MACHINE LEARNING METHODS >>>
    if traditional_ml:
        [train_model(model,model.__class__.__name__, feature.name, feature.xtrain, y_train, feature.xvalid)for model in
        models for feature in featureList]
        cv_tm = pd.DataFrame(entries, columns=['dataset', 'model_name', 'accuracy', 'feature', 'test_nr'])
        cv_tm.to_csv('results/accuracy_table/all_results_from_traditional_training.csv', mode='a', index_label=False,
                     header=False,
                     index=False)
    # <<< TRADITIONAL MACHINE LEARNING METHODS <<<

    # >>> GRID SEARCH FOR FEATURE COUNT VECTORIZER >>>
    xtrain, xtest, ytrain, ytest = train_test_split(train[train_tweet], train[train_label], test_size=0.3)
    # for model in models:
    #     bestfeatures1 = paramCountVectSearch(datasetname,model,xtrain,ytrain)
    #     print(bestfeatures1)
    if param_s_count_vect:
        Parallel()(delayed(paramCountVectSearch)(datasetname,model,xtrain,ytrain) for model in models )
    # <<< GRID SEARCH FOR FEATURE COUNT VECTORIZER <<<

    # >>> GRID SEARCH FOR FEATURE TFIDF VECTORIZER >>>
    xtrain, xtest, ytrain, ytest = train_test_split(train[train_tweet], train[train_label], test_size=0.3)
    # for model in models:
    #     bestfeatures = paramTfidfSearch(datasetname,model,xtrain,ytrain)
    #     print(bestfeatures)
    if params_s_tfidf:
        Parallel()(delayed(paramTfidfSearch)(datasetname,model,xtrain,ytrain) for model in models )
    # <<< GRID SEARCH FOR FEATURE TFIDF VECTORIZER <<<

    # >>> SVC GRID SEARCH >>>
    #[grid_search_svc(feature.xtrain,y_train) for feature in featureList]
    #grid_search_svc(xtrain_count,y_train)
    # <<< SVC GRID SEARCH <<<

    # >>> LABEL ENCODE THE TARGET VARIABLE >>>
    # This block of code is just used for neural networks,
    # cannot be used before traditional machine learning
    encoder = preprocessing.LabelEncoder()
    y_train = encoder.fit_transform(y_train)
    y_test = encoder.fit_transform(y_test)
    num_classes = np.max(y_train) + 1
    y_train = utils.to_categorical(y_train, num_classes)
    y_test = utils.to_categorical(y_test, num_classes)
    # <<< LABEL ENCODE THE TARGET VARIABLE <<<

    # >>> SHALLOW NEURAL NETWORK >>>
    storeresults = []
    if shallow_neural_networks:
        # shallow_neural_network(batch_size=20, epochs=10, dropout=0.1, optimizer="adam", loss="binary_crossentropy", feature_vector_train=y_train, feature_vector_valid=y_test, feature_name="label_encoder")
        # Parallel()(delayed(shallow_neural_network)(batch_size=20, epochs=10, dropout=0.1, optimizer="adam", loss="binary_crossentropy",feature_vector_train=feature.xtrain, feature_vector_valid=feature.xvalid, feature_name=feature.name)  for feature in featureList )
        [shallow_neural_network(batch_size=20, epochs=10, dropout=0.1, optimizer="adam", loss="binary_crossentropy",feature_vector_train=feature.xtrain, feature_vector_valid=feature.xvalid, feature_name=feature.name) for feature in featureList]
    # <<< SHALLOW NEURAL NETWORK <<<

    # >>> DEEP LEARNING >>>
    deep_learnresults=[]
    final_deep_learnresults=[]
    if deep_learning_neural_networks:
        deep_learning_neural_network(hiddenlayers=3,
            batch_size=10,
            epochs=10,
            dropout=0.2,
            optimizer=Nadam(learning_rate=0.1),
            init_mode="glorot_uniform",
            activation="hard_sigmoid",
            weight_constraint=2,
            neurons=20,
            feature_vector_train=y_train,
            feature_vector_valid=y_test,
            feature_name="label_encoder")
        deep_learning_neural_network(hiddenlayers=10,
            batch_size=10,
            epochs=10,
            dropout=0.2,
            optimizer=Nadam(learning_rate=0.1),
            init_mode="glorot_uniform",
            activation="hard_sigmoid",
            weight_constraint=2,
            neurons=20,
            feature_vector_train=y_train,
            feature_vector_valid=y_test,
            feature_name="label_encoder")
    # [deep_learning_neural_network(3,20,10,0.1,"adam","binary_crossentropy",feature.xtrain,feature.xvalid,feature.name) for feature in featureList] # for testing purposes
    # <<< DEEP LEARNING <<<


    # >>> GRID SEARCH DEEP LEARRNING >>>
    store_grid_neural = []
    if neural_grid_seach:
        # neural_network_grid_search(xtrain_count, epochs_and_batch=True, feature_name="count_vectorizer") # for testing purposes

        gridneural = grid_search_neural_network(xtrain_count,y_train,y_test,num_classes)

        # >>> batches and epochs grid search >>>
        batchepoch_dict=gridneural.tune_batches_and_epochs(datasetname,"count_vector")
        epochs = batchepoch_dict.get("epochs")
        batch_size = batchepoch_dict.get("batch_size")
        # <<< batches and epochs grid search <<<

        # >>> optimzer grid search >>>
        optimim_dict=gridneural.tune_optimization(datasetname,"count_vector",epochs=epochs,batch_size=batch_size)
        optimizer = optimim_dict.get("optimizer")
        # <<< optimizer grid search >>>

        # >>> learning rate and momentum grid search >>>
        learnrate_momentum_dict=gridneural.tune_learningrate_and_momentum(datasetname, "count_vector", epochs=epochs,batch_size=batch_size)
        learn_rate = learnrate_momentum_dict.get("learn_rate")
        momentum = learnrate_momentum_dict.get("momentum")
        # <<< learning rate and momentum grid search <<<

        # >>> network weight initialization grid search >>>
        weight_dict = gridneural.tune_network_weight_initialization(datasetname,"count_vector",epochs=epochs,batch_size=batch_size,optimizer=optimizer)
        init_mode = weight_dict.get("init_mode")
        # <<< network weight initialization grid search <<<

        # >>> neuron activation grid search >>>
        # activation_dict = gridneural.tune_neuron_activation(datasetname,"count_vector", epochs=epochs,batch_size=batch_size,optimizer=optimizer,weight_init=init_mode)
        # activation = activation_dict.get("activation")
        # <<< neuron activation grid search <<<

        # >>> dropout regularization grid search >>>
        dropout_dict= gridneural.tune_dropout_regularization(datasetname,"count_vector", epochs=epochs,batch_size=batch_size,optimizer=optimizer,init_mode=init_mode,activation=activation)
        dropout_rate = dropout_dict.get("dropout_rate")
        # <<< dropout regularization grid search <<<

        # >>> neurons in layer grid search >>>
        neuron_dict = gridneural.tune_neurons_in_layer(datasetname,"count_vector",epochs=epochs,batch_size=batch_size,optimizer=optimizer,init_mode=init_mode,activation=activation,dropout_rate=dropout_rate)
        neurons = neuron_dict.get("neurons")
        # <<< neurons in layer grid search <<<
    # <<< GRID SEARCH DEEP LEARNING >>>

    # >>> WRITE RESULTS >>>
    cv_at = pd.DataFrame(storeresults, columns=['dataset','model_name','accuracy','feature','epochs', 'batch_size', 'droput','optimizer','loss'])
    cv_dl= pd.DataFrame(deep_learnresults,columns=['dataset','model_name','accuracy','feature','hidden_layers','epochs', 'batch_size', 'droput','optimizer','loss'])
    cv_fdl = pd.DataFrame(final_deep_learnresults,columns=['dataset','model_name','accuracy','feature','hidden_layers','epochs', 'batch_size', 'droput','optimizer'])
    cv_dgrid= pd.DataFrame(store_grid_neural, columns=['dataset','model_name','test_type','feature_name', 'grid_result.best_score', 'grid_result.best_params'])

    cv_at.to_csv('results/accuracy_table/shallow_neural_network.csv', mode='a', index_label=False, header=False,index=False)
    cv_dl.to_csv('results/accuracy_table/shallow_neural_network.csv', mode='a', index_label=False, header=False,index=False)
    cv_fdl.to_csv('results/accuracy_table/final_deep_learning.csv', mode='a', index_label=False, header=False, index=False)
    cv_dgrid.to_csv('results/accuracy_table/all_results_from_deep_learning_gridsearch.csv', mode='a', index_label=False,header=False, index=False)
    # <<< WRITE RESULTS <<<


    end = time.time()
    temp = end - start
    hours = temp // 3600
    temp = temp - 3600 * hours
    minutes = temp // 60
    seconds = temp - 60 * minutes
    print('The overall time spent on the whole process: %d hours, %d minutes, %d seconds' % (hours, minutes, seconds))