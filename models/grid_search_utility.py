# grid_search_utility.py
# SENTIMENT ANALYSIS - GRID SEARCH UTILITY
# AUTHOR: KAMIL LIPSKI
# YEAR: 2019 - 2021

# RESOURCES:
# https://machinelearningmastery.com/grid-search-hyperparameters-deep-learning-models-python-keras/

# DESCRIPTION:
# This utility will help you perform parameter search on different features and neural networks
 
# >>> IMPORTS >>>
import time
import numpy as np
import pandas as pd
import pandasql as ps
from keras import Sequential, utils
from keras.constraints import maxnorm
from keras.layers import Activation, Dense, Dropout
from keras.optimizers import SGD
from keras.wrappers.scikit_learn import KerasClassifier
from sklearn import preprocessing
from sklearn.feature_extraction import FeatureHasher
from sklearn.feature_extraction.text import (CountVectorizer,TfidfVectorizer)
from sklearn.linear_model import SGDClassifier
from sklearn.metrics import accuracy_score, classification_report
from sklearn.model_selection import GridSearchCV, RandomizedSearchCV, train_test_split
from sklearn.pipeline import Pipeline
from sklearn.svm import SVC
from tabulate import tabulate
import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning)
# <<< IMPORTS <<<

# >>> HYPER PARAMETER SEARCH OF SVM>>>
class paramTunSDG:
    def __init__(self,X,y):
        # Split the dataset in two equal parts

        # Split the dataset in two equal parts
        X_train, X_test, y_train, y_test = train_test_split(
            X, y, test_size=0.5, random_state=0)

        # Set the parameters by cross-validation
        tuned_parameters = [{'kernel': ['rbf'], 'gamma': [1e-3, 1e-4],
                             'C': [1, 10, 100, 1000]},
                            {'kernel': ['linear'], 'C': [1, 10, 100, 1000]}]

        scores = ['precision', 'recall']

        for score in scores:
            print("# Tuning hyper-parameters for %s" % score)
            print()

            clf = GridSearchCV(SGDClassifier(), tuned_parameters, cv=5,
                               scoring='%s_macro' % score)
            clf.fit(X_train, y_train)

            print("Best parameters set found on development set:")
            print()
            print(clf.best_params_)
            print()
            print("Grid scores on development set:")
            print()
            means = clf.cv_results_['mean_test_score']
            stds = clf.cv_results_['std_test_score']
            for mean, std, params in zip(means, stds, clf.cv_results_['params']):
                print("%0.3f (+/-%0.03f) for %r"
                      % (mean, std * 2, params))
            print()

            print("Detailed classification report:")
            print()
            print("The model is trained on the full development set.")
            print("The scores are computed on the full evaluation set.")
            print()
            y_true, y_pred = y_test, clf.predict(X_test)
            print(classification_report(y_true, y_pred))
            print()

# Utility function to report best scores
def report(results, n_top=3):
    for i in range(1, n_top + 1):
        candidates = np.flatnonzero(results['rank_test_score'] == i)
        for candidate in candidates:
            print("Model with rank: {0}".format(i))
            print("Mean validation score: {0:.3f} (std: {1:.3f})".format(
                results['mean_test_score'][candidate],
                results['std_test_score'][candidate]))
            print("Parameters: {0}".format(results['params'][candidate]))
            print("")

tuned_parameters = [{'kernel': ['rbf'], 'gamma': [1e-2, 1e-1, 1], 'C': [1, 10, 100]},
                    {'kernel': ['linear'], 'C': [1, 10, 100]},
                    {'kernel': ['poly'], 'degree': [2, 3, 4], 'coef0': [0, 1], 'gamma': [1e-2, 1e-1, 1],
                     'C': [1, 10, 100]}]

class grid_search_svc:
    def __init__(self,train_x,train_y):
        # run rbf randomized search
        start = time()
        rbf_grid_search = GridSearchCV(SVC(), tuned_parameters[0], cv=5)

        rbf_grid_search.fit(train_x, train_y)
        print("RBF RandomizedSearchCV took %.2f seconds for %d candidates"
              " parameter settings." % ((time() - start), len(rbf_grid_search.cv_results_['params'])))
        report(rbf_grid_search.cv_results_)

        # run linear randomized
        start = time()
        linear_grid_search = GridSearchCV(SVC(), tuned_parameters[1], cv=5)

        linear_grid_search.fit(train_x, train_y)
        print("LINEAR RandomizedSearchCV took %.2f seconds for %d candidates"
              " parameter settings." % ((time() - start), len(linear_grid_search.cv_results_['params'])))
        report(rbf_grid_search.cv_results_)

        # run poly randomized search
        start = time()
        n_iter_search = 20
        poly_random_search = RandomizedSearchCV(SVC(), tuned_parameters[2], cv=5,
                                                n_iter=n_iter_search)

        poly_random_search.fit(train_x, train_y)
        print("POLY RandomizedSearchCV took %.2f seconds for %d candidates"
              " parameter settings." % ((time() - start), n_iter_search))
        report(poly_random_search.cv_results_)
# <<< HYPER PARAMETER SEARCH OF SVC <<<

# >>> HYPER PARAMETER SEARCH OF COUNT VECTORIZER >>>
class paramCountVectSearch:
    def __new__(cls, datasetname, classifier, xtrain, ytrain, latex=False):
        print("Running method:",cls.__name__,", dataset: ",datasetname,"\nStarting parameter optimization for "
                                                                       "feature CountVectorizer() "
                                             "with classifier ",classifier.__class__.__name__ , )
        start = time.time()
        cls.datasetname =datasetname
        cls.ytrain = ytrain
        cls.classfier = classifier
        cls.xtrain = xtrain

        clf_name=str(classifier.__class__.__name__)
        all_results = pd.read_csv(
            "results/accuracy_table/gridsearch_features/vect/all_results_from_feature_gridsearch.csv")
        query = """ select test_nr from all_results where test_nr is not Null order by test_nr desc limit 1 """
        df = ps.sqldf(query, locals())
        new_test_nr = 0
        if (df.empty == True):
            new_test_nr = 1
        else:
            lasttestnr = df['test_nr'][df.index[-1]]
            new_test_nr = lasttestnr + 1

        pipeline = Pipeline([
            ('vect',CountVectorizer()),
            ('clf',classifier)
        ])

        param_grid = {
            'vect__max_df': (0.5, 0.75, 1.0),
            'vect__lowercase': [True, False],
            'vect__ngram_range': [(1, 1), (1, 2), (1, 3)],
            'vect__analyzer': ['word','char','char_wb']
        }

        grid = GridSearchCV(pipeline,param_grid,cv=5, n_jobs=6)
        grid_result = grid.fit(cls.xtrain, cls.ytrain)

        # summarize results
        best_result = []
        best_params = grid_result.best_params_
        best_score = grid_result.best_score_
        best_result.append((datasetname,classifier.__class__.__name__,best_params['vect__analyzer'],
                            best_params['vect__ngram_range'],best_params['vect__lowercase'],
                            best_params['vect__max_df'],best_score,new_test_nr))
        columns = ['dataset','model_name','vect__analyzer','vect__ngram_range',
                                                    'vect__lower_case','vect__max_df','accuracy','test_nr']
        cv_best = pd.DataFrame(best_result, columns=columns)
        cv_best.to_csv('results/accuracy_table/gridsearch_features/vect/best_results_from_feature_gridsearch.csv',
               columns=columns, mode='a', index_label=False, header=False, index=False)
        means = grid_result.cv_results_['mean_test_score']
        stds = grid_result.cv_results_['std_test_score']
        params = grid_result.cv_results_['params']

        all_results = []
        for mean, stdev, param in zip(means, stds, params):
            all_results.append((datasetname,classifier.__class__.__name__, param['vect__analyzer'],
                                param['vect__ngram_range'], param['vect__lowercase'], param['vect__max_df'],
                                mean,new_test_nr))
            #print("%f (%f) with: %r" % (mean, stdev, param))

        cv_all = pd.DataFrame(all_results, columns=['dataset','model_name','vect__analyzer','vect__ngram_range',
                                                    'vect__lower_case','vect__max_df','accuracy','test_nr'])
        cv_all.to_csv('results/accuracy_table/gridsearch_features/vect/all_results_from_feature_gridsearch.csv',
                columns=columns, mode='a', index_label=False, header=False,index=False)
        end = time.time()
        result = (end-start) / 60

        newest_all_results = pd.read_csv(
            "results/accuracy_table/gridsearch_features/vect/all_results_from_feature_gridsearch.csv")

        q2 = "select * from newest_all_results where model_name='"+clf_name+"' and test_nr='"+str(new_test_nr)+\
             "' order by accuracy desc"


        # This one will print out results in ready latex format
        if latex:
            print(tabulate(ps.sqldf(q2, locals()), headers='keys', tablefmt='latex'))

        print(tabulate(ps.sqldf(q2, locals()), headers='keys', tablefmt='psql'))
        print("Best: %f using %s" % (grid_result.best_score_, grid_result.best_params_))
        print("Done running method: ", cls.__name__, ", dataset: ", datasetname,
              "\nTime spent on parameter optimization for feature CountVectorizer() with classifier ",
              classifier.__class__.__name__, " and dataset ", datasetname, ": ", "%.2f" % result, " minutes\n")

        count_feat = {
            'model': classifier,
            'name': 'CountVectorizer',
            'parameters': best_params,
            'accuracy': best_score,
            'function': 'CountVectorizer(analyzer="' + str(best_params.get('vect__analyzer')) + '",ngram_range="' +
                        str(best_params.get('vect__ngram_range')) + '", lowercase="' + str(
                best_params.get('vect__lowercase')) +
                        '", max_df="' + str(best_params.get('vect__max_df')) + '")',
            'test_nr': new_test_nr
        }

        return count_feat
# <<< HYPER PARAMETER SEARCH OF COUNT VECTORIZER <<<

# >>> HYPER PARAMETER SEARCH OF TFIDF VECTORIZER >>>
class paramTfidfSearch:
    import matplotlib.pyplot as plt
    import numpy as np
    def __new__(cls, datasetname, classifier, xtrain, ytrain, latex=False):
        import matplotlib.pyplot as plt
        print("Running method:",cls.__name__,", dataset: ",datasetname,
              "\nStarting parameter optimization for feature TfidfVectorizer() "
                                             "with classifier ",classifier.__class__.__name__ , )
        start = time.time()
        cls.datasetname=datasetname
        cls.ytrain = ytrain
        cls.classfier = classifier
        cls.xtrain = xtrain

        clf_name = str(classifier.__class__.__name__)
        all_results = pd.read_csv(
            "results/accuracy_table/gridsearch_features/tfidf/all_results_from_feature_gridsearch.csv")
        query = """ select test_nr from all_results where test_nr is not Null order by test_nr desc limit 1 """
        df = ps.sqldf(query, locals())
        new_test_nr = 0
        if (df.empty == True):
            new_test_nr = 1
        else:
            lasttestnr = df['test_nr'][df.index[-1]]
            new_test_nr = lasttestnr + 1

        pipeline = Pipeline([
            ('tfidf',TfidfVectorizer(sublinear_tf=True)),
            ('clf',classifier)
        ])

        param_grid = {
            'tfidf__use_idf': [True,False],
            'tfidf__lowercase': [True, False],
            'tfidf__ngram_range': [(1, 1), (1, 2), (1, 3)],
            'tfidf__analyzer': ['word','char','char_wb']
        }

        grid = GridSearchCV(pipeline,param_grid,cv=5,n_jobs=10)
        grid_result = grid.fit(cls.xtrain, cls.ytrain)

        # summarize results
        best_result = []
        best_params = grid_result.best_params_
        best_score = grid_result.best_score_
        best_result.append((datasetname,classifier.__class__.__name__,best_params['tfidf__analyzer'],
                            best_params['tfidf__ngram_range'],best_params['tfidf__lowercase'],
                            best_params['tfidf__use_idf'],best_score,new_test_nr))
        columns = ['dataset','model_name','tfidf__analyzer','tfidf__ngram_range',
                                                    'tfidf__lower_case','tfidf__use_idf','accuracy','test_nr']
        cv_best = pd.DataFrame(best_result, columns=columns)
        cv_best.to_csv('results/accuracy_table/gridsearch_features/tfidf/best_results_from_feature_gridsearch.csv',
               mode='a', columns=columns, index_label=False, header=False, index=False)
        means = grid_result.cv_results_['mean_test_score']
        stds = grid_result.cv_results_['std_test_score']
        params = grid_result.cv_results_['params']

        all_results = []
        for mean, stdev, param in zip(means, stds, params):
            all_results.append((datasetname,classifier.__class__.__name__, param['tfidf__analyzer'],
                                param['tfidf__ngram_range'], param['tfidf__lowercase'], param['tfidf__use_idf'],
                                mean,new_test_nr))
            #print("%f (%f) with: %r" % (mean, stdev, param))

        cv_all = pd.DataFrame(all_results, columns=columns)
        cv_all.to_csv('results/accuracy_table/gridsearch_features/tfidf/all_results_from_feature_gridsearch.csv',
                      mode='a', index_label=False, header=False,index=False)
        end = time.time()
        result = (end-start) / 60

        newest_all_results = pd.read_csv(
            "results/accuracy_table/gridsearch_features/tfidf/all_results_from_feature_gridsearch.csv")

        q2 = "select * from newest_all_results where model_name='" + clf_name + "' and test_nr='" + str(new_test_nr) + \
             "' order by accuracy desc"


        # This one will print out results in ready latex format
        if latex:
            print(tabulate(ps.sqldf(q2, locals()), headers='keys', tablefmt='latex'))

        print(tabulate(ps.sqldf(q2, locals()), headers='keys', tablefmt='psql'))
        print("Best: %f using %s" % (grid_result.best_score_, grid_result.best_params_))
        print("Done running method: ",cls.__name__,", dataset: ",datasetname,
              "\nTime spent on parameter optimization for feature TfidfVectorizer() with classifier ",
              classifier.__class__.__name__, " and dataset ",datasetname, ": ", "%.2f" % result ," minutes\n")

        tfidf_feat = {
            'model': classifier,
            'name': 'TfidfVectorizer',
            'parameters': best_params,
            'accuracy': best_score,
            'function': 'TfidfVectorizer(analyzer="'+str(best_params.get('tfidf__analyzer'))+'",ngram_range="'+
                        str(best_params.get('tfidf__ngram_range'))+'", lowercase="'+str(best_params.get('tfidf__lowercase'))+
                        '", use_idf="'+str(best_params.get('tfidf__use_idf'))+'")',
            'test_nr': new_test_nr
        }

        return tfidf_feat
# <<< HYPER PARAMETER SEARCH OF TFIDF VECTORIZER <<<


# >>> HYPER PARAMETER SEARCH OF NEURAL NETWORK >>>
store_batches_epochs = []
store_learr_momentu = []
store_netweight = []
store_neuron_activation = []

class grid_search_neural_network:
    def __init__(self,x_train,y_train,y_test,num_classes):
        self.x_train = x_train
        self.y_train = y_train
        self.y_test = y_test
        self.num_classes = num_classes

    def tune_batches_and_epochs(self, datasetname,feature_name,epochs=(50,100,150),batches=(5,10,20)):
        print("Process of tuning batches and epochs starts...")
        def create_model():
            # Build the model
            model = Sequential()
            input_dim = self.x_train.shape[1]
            model.add(Dense(512, input_dim=input_dim))
            model.add(Dense(self.num_classes))
            model.add(Dropout(0.1))
            model.add(Activation('softmax'))

            model.compile(loss="binary_crossentropy", optimizer="adam",
                          metrics=['accuracy'])
            return model

        model = KerasClassifier(build_fn=create_model)
        param_grid = dict(epochs=epochs,batch_size=batches)
        grid = GridSearchCV(estimator=model,
                            param_grid=param_grid)
        grid_result = grid.fit(self.x_train, self.y_train)

        # summarize results
        print("Best: %f using %s" % (grid_result.best_score_, grid_result.best_params_))

        store_batches_epochs.append((datasetname, "neural_network", "epochs_and_batchsize", feature_name,
                                     grid_result.best_score_, grid_result.best_params_))
        means = grid_result.cv_results_['mean_test_score']
        stds = grid_result.cv_results_['std_test_score']
        params = grid_result.cv_results_['params']

        all_results = []
        for mean, stdev, param in zip(means, stds, params):
            print("%f (%f) with: %r" % (mean, stdev, param))
            all_results.append((datasetname, "neural_network", "epochs_and_batchsize", feature_name, mean, param))

        columns = ['dataset', 'model_name', 'test_type', 'feature_name', 'best_score', 'best_params']
        cv_dgrid = pd.DataFrame(store_batches_epochs, columns=columns)
        cv_dgrid.to_csv('results/accuracy_table/best_results_from_deep_learning_gridsearch.csv', mode='a', index_label=False, header=False, index=False)
        cv_all = pd.DataFrame(all_results, columns=columns)
        cv_all.to_csv('results/accuracy_table/all_results_from_deep_learning_gridsearch.csv', mode='a', index_label=False, header=False, index=False)
        return grid_result.best_params_

    def tune_optimization(self,datasetname,feature_name,epochs,batch_size):
        print("Process of tuning optimizer starts...")
        def create_model(optimizer="adam"):
            # Build the model
            model = Sequential()
            input_dim = self.x_train.shape[1]
            model.add(Dense(512, input_dim=input_dim))
            model.add(Dense(self.num_classes))
            model.add(Dropout(0.1))
            model.add(Activation('softmax'))
            model.compile(loss="binary_crossentropy",optimizer=optimizer, metrics=['accuracy'])
            return model
        model = KerasClassifier(build_fn=create_model, epochs=epochs, batch_size=batch_size)
        optimizer = ['SGD', 'RMSprop', 'Adagrad', 'Adadelta', 'Adam', 'Adamax', 'Nadam']
        param_grid=dict(optimizer=optimizer)
        grid = GridSearchCV(estimator=model, param_grid=param_grid)
        grid_result = grid.fit(self.x_train,self.y_train)
        # summarize results
        print("Best: %f using %s" % (grid_result.best_score_, grid_result.best_params_))

        store_batches_epochs.append((datasetname, "neural_network", "optimizer", feature_name, grid_result.best_score_, grid_result.best_params_))
        means = grid_result.cv_results_['mean_test_score']
        stds = grid_result.cv_results_['std_test_score']
        params = grid_result.cv_results_['params']

        all_results = []
        for mean, stdev, param in zip(means, stds, params):
            print("%f (%f) with: %r" % (mean, stdev, param))
            all_results.append((datasetname, "neural_network", "optimizer", feature_name, mean, param))

        columns = ['dataset', 'model_name', 'test_type', 'feature_name', 'best_score','best_params']
        cv_dgrid = pd.DataFrame(store_batches_epochs, columns=columns)
        cv_dgrid.to_csv('results/accuracy_table/best_results_from_deep_learning_gridsearch.csv', mode='a', index_label=False, header=False, index=False)
        cv_all = pd.DataFrame(all_results, columns=columns)
        cv_all.to_csv('results/accuracy_table/all_results_from_deep_learning_gridsearch.csv', mode='a', index_label=False, header=False, index=False)
        return grid_result.best_params_

    def tune_learningrate_and_momentum(self,datasetname,feature_name, epochs,batch_size, learn_rate= [0.001, 0.01, 0.1, 0.2, 0.3],momentum = [0.0, 0.2, 0.4, 0.6, 0.8, 0.9]):
        print("Process of tuning learning rate and momentum starts...")
        def create_model(learn_rate=0.01, momentum=0):
            model = Sequential()
            input_dim = self.x_train.shape[1]
            model.add(Dense(512, input_dim=input_dim))
            model.add(Dense(self.num_classes))
            model.add(Dropout(0.1))
            model.add(Activation('softmax'))
            # Compile model
            optimizer = SGD(lr=learn_rate, momentum=momentum)
            model.compile(loss='binary_crossentropy', optimizer=optimizer, metrics=['accuracy'])
            return model

        # fix random seed for reproducibility
        seed = 7
        np.random.seed(seed)
        model = KerasClassifier(build_fn=create_model, epochs=epochs, batch_size=batch_size)


        param_grid = dict(learn_rate=learn_rate, momentum=momentum)
        grid = GridSearchCV(estimator=model, param_grid=param_grid)
        grid_result = grid.fit(self.x_train, self.y_train)
        # summarize results
        print("Best: %f using %s" % (grid_result.best_score_, grid_result.best_params_))
        store_learr_momentu.append((datasetname, "neural_network", "learning_rate_and_momentum", feature_name, grid_result.best_score_, grid_result.best_params_))
        means = grid_result.cv_results_['mean_test_score']
        stds = grid_result.cv_results_['std_test_score']
        params = grid_result.cv_results_['params']

        all_results=[]
        for mean, stdev, param in zip(means, stds, params):
            print("%f (%f) with: %r" % (mean, stdev, param))
            all_results.append((datasetname, "neural_network", "learning_rate_and_momentum", feature_name, mean, param))

        columns=['dataset', 'model_name', 'test_type', 'feature_name', 'best_score','best_params']
        cv_lm = pd.DataFrame(store_learr_momentu, columns=columns)
        cv_lm.to_csv('results/accuracy_table/best_results_from_deep_learning_gridsearch.csv', mode='a', index_label=False, header=False, index=False)
        cv_all = pd.DataFrame(all_results, columns=columns)
        cv_all.to_csv('results/accuracy_table/all_results_from_deep_learning_gridsearch.csv', mode='a', index_label=False, header=False, index=False)
        return grid_result.best_params_

    def tune_network_weight_initialization(self, datasetname, feature_name, epochs, batch_size, optimizer, weight_init=['uniform', 'lecun_uniform', 'normal', 'zero', 'glorot_normal', 'glorot_uniform', 'he_normal',
                     'he_uniform']):
        print("Process of tuning network weight starts...")
        def create_model(init_mode="uniform"):
            model = Sequential()
            input_dim = self.x_train.shape[1]
            model.add(Dense(512, input_dim=input_dim, kernel_initializer=init_mode, activation="hard_sigmoid"))
            model.add(Dense(self.num_classes, kernel_initializer=init_mode,activation="sigmoid"))
            model.add(Dropout(0.1))
            model.add(Activation('softmax'))
            model.compile(loss='binary_crossentropy', optimizer=optimizer, metrics=['accuracy'])
            return model

        seed = 7
        np.random.seed(seed)
        model = KerasClassifier(build_fn=create_model, epochs=epochs, batch_size=batch_size)
        init_mode = weight_init
        param_grid = dict(init_mode=init_mode)
        grid = GridSearchCV(estimator=model, param_grid=param_grid)
        grid_result = grid.fit(self.x_train, self.y_train)
        # summarize results
        print("Best: %f using %s" % (grid_result.best_score_, grid_result.best_params_))
        store_netweight.append((datasetname, "neural_network", "weight_initialization", feature_name, grid_result.best_score_, grid_result.best_params_))
        means = grid_result.cv_results_['mean_test_score']
        stds = grid_result.cv_results_['std_test_score']
        params = grid_result.cv_results_['params']

        all_results = []
        for mean, stdev, param in zip(means, stds, params):
            print("%f (%f) with: %r" % (mean, stdev, param))
            all_results.append((datasetname, "neural_network", "weight_initialization", feature_name, mean, param))

        columns=['dataset', 'model_name', 'test_type', 'feature_name', 'best_score', 'best_params']
        cv_nw = pd.DataFrame(store_netweight, columns=columns)
        cv_nw.to_csv('results/accuracy_table/best_results_from_deep_learning_gridsearch.csv', mode='a', index_label=False, header=False, index=False)
        cv_all = pd.DataFrame(all_results, columns=columns)
        cv_all.to_csv('results/accuracy_table/all_results_from_deep_learning_gridsearch.csv', mode='a', index_label=False, header=False, index=False)
        return grid_result.best_params_

    def tune_neuron_activation(self,datasetname,feature_name,epochs,batch_size,optimizer,weight_init,activation = ['softmax', 'softplus', 'softsign', 'relu', 'tanh', 'sigmoid', 'hard_sigmoid', 'linear']):
        print("Process of tuning neuron activation starts...")
        def create_model(activation='relu'):
            model = Sequential()
            input_dim = self.x_train.shape[1]
            model.add(Dense(512, input_dim=input_dim, kernel_initializer=weight_init,activation=activation))
            model.add(Dense(self.num_classes, kernel_initializer=weight_init,activation="sigmoid"))
            model.add(Dropout(0.1))
            model.compile(loss='binary_crossentropy', optimizer=optimizer, metrics=['accuracy'])
            return model

        seed = 7
        np.random.seed(seed)
        model = KerasClassifier(build_fn=create_model, epochs=epochs, batch_size=batch_size)
        activation = activation
        param_grid = dict(activation=activation)
        grid = GridSearchCV(estimator=model, param_grid=param_grid)
        grid_result = grid.fit(self.x_train, self.y_train)
        # summarize results
        print("Best: %f using %s" % (grid_result.best_score_, grid_result.best_params_))
        store_neuron_activation.append((datasetname, "neural_network", "activation", feature_name, grid_result.best_score_, grid_result.best_params_))

        means = grid_result.cv_results_['mean_test_score']
        stds = grid_result.cv_results_['std_test_score']
        params = grid_result.cv_results_['params']

        all_results = []
        for mean, stdev, param in zip(means, stds, params):
            print("%f (%f) with: %r" % (mean, stdev, param))
            all_results.append((datasetname, "neural_network", "activation", feature_name, mean, param))

        columns=['dataset', 'model_name', 'test_type', 'feature_name', 'best_score', 'best_params']
        cv_na = pd.DataFrame(store_neuron_activation, columns=columns)
        cv_na.to_csv('results/accuracy_table/best_results_from_deep_learning_gridsearch.csv', mode='a', index_label=False, header=False, index=False)
        cv_all = pd.DataFrame(all_results, columns=columns)
        cv_all.to_csv('results/accuracy_table/all_results_from_deep_learning_gridsearch.csv', mode='a', index_label=False, header=False, index=False)
        return grid_result.best_params_

    def tune_dropout_regularization(self,datasetname,feature_name,epochs,batch_size,optimizer,init_mode,activation,
                                    weight_constraint = [1, 2, 3, 4, 5],
                                    dropout_rate = [0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]):
        print("Process of tuning dropout regularization starts...")
        def create_model(dropout_rate=0.0, weight_constraint=0):
            # create model
            model = Sequential()
            input_dim = self.x_train.shape[1]
            model.add(Dense(512, input_dim=input_dim, kernel_initializer=init_mode, activation=activation,
                            kernel_constraint=maxnorm(weight_constraint)))
            model.add(Dropout(dropout_rate))
            model.add(Dense(self.num_classes, kernel_initializer=init_mode, activation='sigmoid'))
            # Compile model
            model.compile(loss='binary_crossentropy', optimizer=optimizer, metrics=['accuracy'])
            return model
        seed = 7
        np.random.seed(seed)
        model = KerasClassifier(build_fn=create_model, epochs=epochs,batch_size=batch_size)
        # define the grid search parameters
        weight_constraint = weight_constraint
        dropout_rate = dropout_rate
        param_grid = dict(dropout_rate=dropout_rate, weight_constraint=weight_constraint)
        grid = GridSearchCV(estimator=model, param_grid=param_grid)
        grid_result = grid.fit(self.x_train, self.y_train)
        # summarize results
        print("Best: %f using %s" % (grid_result.best_score_, grid_result.best_params_))
        store_neuron_activation.append((datasetname, "neural_network", "dropout", feature_name,
                                        grid_result.best_score_, grid_result.best_params_))

        means = grid_result.cv_results_['mean_test_score']
        stds = grid_result.cv_results_['std_test_score']
        params = grid_result.cv_results_['params']

        all_results = []
        for mean, stdev, param in zip(means, stds, params):
            print("%f (%f) with: %r" % (mean, stdev, param))
            all_results.append((datasetname, "neural_network", "dropout", feature_name, mean, param))

        columns=['dataset', 'model_name', 'test_type', 'feature_name', 'best_score','best_params']
        cv_dr = pd.DataFrame(store_neuron_activation, columns=columns)
        cv_dr.to_csv('results/accuracy_table/best_results_from_deep_learning_gridsearch.csv', mode='a',index_label=False,header=False, index=False)
        cv_all = pd.DataFrame(all_results, columns=columns)
        cv_all.to_csv('results/accuracy_table/all_results_from_deep_learning_gridsearch.csv', mode='a', index_label=False, header=False, index=False)
        return grid_result.best_params_

    def tune_neurons_in_layer(self,datasetname, feature_name,epochs,batch_size,optimizer,init_mode,activation,dropout_rate,neurons=[1,5,10,15,20,25,30]):
        print("Process of tuning neurons in layer starts...")
        def create_model(neurons=1):
            model=Sequential()
            input_dim= self.x_train.shape[1]
            model.add(Dense(neurons,input_dim=input_dim,kernel_initializer=init_mode, activation=activation))
            model.add(Dropout(dropout_rate))
            model.add(Dense(self.num_classes, kernel_initializer=init_mode,activation="sigmoid"))
            model.compile(loss='binary_crossentropy', optimizer=optimizer, metrics=['accuracy'])
            return model
        seed = 7
        np.random.seed(seed)
        model = KerasClassifier(build_fn=create_model,epochs=epochs,batch_size=batch_size)
        neurons=neurons
        param_grid=dict(neurons=neurons)
        grid = GridSearchCV(estimator=model, param_grid=param_grid)
        grid_result = grid.fit(self.x_train,self.y_train)
        # summarize results
        print("Best: %f using %s" % (grid_result.best_score_, grid_result.best_params_))
        store_neuron_activation.append((datasetname, "neural_network", "neurons", feature_name,
                                        grid_result.best_score_, grid_result.best_params_))

        means = grid_result.cv_results_['mean_test_score']
        stds = grid_result.cv_results_['std_test_score']
        params = grid_result.cv_results_['params']

        all_results = []
        for mean, stdev, param in zip(means, stds, params):
            print("%f (%f) with: %r" % (mean, stdev, param))
            all_results.append((datasetname, "neural_network", "neurons", feature_name, mean, param))

        columns=['dataset', 'model_name', 'test_type', 'feature_name', 'best_score','best_params']
        cv_ne = pd.DataFrame(store_neuron_activation, columns=columns)
        cv_ne.to_csv('results/accuracy_table/best_results_from_deep_learning_gridsearch.csv', mode='a',index_label=False, header=False, index=False)
        cv_all = pd.DataFrame(all_results, columns=columns)
        cv_all.to_csv('results/accuracy_table/all_results_from_deep_learning_gridsearch.csv', mode='a', index_label=False, header=False, index=False)
        return grid_result.best_params_
# <<< HYPER PARAMETER SEARCH OF NEURAL NETWORK <<<
