# tweet_cleaner.py
# SENTIMENT ANALYSIS - TWEET CLEANER
# AUTHOR: KAMIL LIPSKI
# YEAR: 2019 - 2021

# RESOURCES:
# https://www.analyticsvidhya.com/blog/2018/07/hands-on-sentiment-analysis-dataset-python/
# https://www.analyticsvidhya.com/blog/2018/02/the-different-methods-deal-text-data-predictive-python/
# https://machinelearningmastery.com/prepare-movie-review-data-sentiment-analysis/

# DESCRIPTION:
# Basic text-preprocessing techniques for cleaning text data
# Lower casing
# Punctuation removal
# Stopwords removal
# Frequent words removal
# Rare words removal
# Spelling correction
# Tokenization
# Stemming
# Lemmatization

# >>> IMPORTS >>>
import pandas as pd
import numpy as np
import nltk
nltk.download('wordnet')
from nltk.corpus import stopwords
nltk.download('stopwords')
from nltk.stem.porter import *
from textblob import Word
import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning)
# <<< IMPORTS <<<

# >>> NOISE REMOVAL >>>
def noise(dataset, tweetcolumn):
    """ With this method we remove twitter handles (@user),
    we also are removing punctuations, numbers, and special characters
    """
    def remove_pattern(input_txt, pattern):
        r = re.findall(pattern, input_txt)
        for i in r:
            input_txt = re.sub(i, '', input_txt)
        return input_txt

    dataset[tweetcolumn] = dataset[tweetcolumn].str.replace("[^a-zA-Z#]", " ")
    dataset[tweetcolumn] = np.vectorize(remove_pattern)(dataset[tweetcolumn], "@[\w]*")
    return dataset
# <<< NOISE REMOVAL <<<

# >>> SHORT WORDS REMOVAL >>>
def short_words(dataset, tweetcolumn):
    """ this method removes words shorter than 3 letters
    """
    dataset[tweetcolumn] = dataset[tweetcolumn].apply(lambda x: ' '.join([w for w in x.split() if len(w)>3]))
    return dataset
# <<< SHORT WORDS REMOVAL <<<

# >>> STOP WORDS REMOVAL >>>
def stop_words(dataset, tweetcolumn):
    """ this method removes commonly occuring words in english, with use of predefined library stopwrds from nltk.corpus """
    stop = stopwords.words('english')
    dataset[tweetcolumn] = dataset[tweetcolumn].apply(lambda x: " ".join(x for x in x.split() if x not in stop))
    return dataset
# <<< STOP WORDS REMOVAL <<<

# >>> RARE WORDS REMOVAL >>>
def rare_words(dataset, tweetcolumn):
    """ This method removes 10 most rarest occurring words in our text data """
    freq = pd.Series(' '.join(dataset[tweetcolumn]).split()).value_counts()[-10:]
    freq = list(freq.index)
    dataset[tweetcolumn] = dataset[tweetcolumn].apply(lambda x: " ".join(x for x in x.split() if x not in freq))
    return dataset
# <<< RARE WORDS REMOVAL <<<

# >>> COMMON WORDS REMOVAL >>>
def common_words(dataset, tweetcolumn):
    """ This method removes 10 most frequently occurring words in our text data """
    freq = pd.Series(' '.join(dataset[tweetcolumn]).split()).value_counts()[:10]
    freq = list(freq.index)
    dataset[tweetcolumn] = dataset[tweetcolumn].apply(lambda x: " ".join(x for x in x.split() if x not in freq))
    return dataset
# <<< COMMON WORDS REMOVAL <<<

# >>> CONVERTING INTO TOKENS >>>
def tokenization(dataset, tweetcolumn):
    """ Tokens are individual terms or words, and tokenization is the process of splitting a string of text into tokens """
    datas = dataset[tweetcolumn].apply(lambda x: x.split())
    return datas
# <<< CONVERTING INTO TOKENS <<<

# >>> STRIPPING SUFFIXES >>>
def stemming(dataset, tweetcolumn):
    """ Stemming is a rule-based process of stripping the suffixes (“ing”, “ly”, “es”, “s” etc) from a word.
    For example, For example – “play”, “player”, “played”, “plays” and “playing” are the different variations of the word – “play”.
    """
    stemmer = PorterStemmer()
    tokenized_tweet = tokenization(dataset, tweetcolumn)
    tokenized_tweet.apply(lambda x: [stemmer.stem(i) for i in x])  # stemming

    for i in range(len(tokenized_tweet)):
        tokenized_tweet[i] = ' '.join(tokenized_tweet[i])
    return dataset
# <<< STRIPPING SUFFIXIES <<<

# >>> CONVERTING INTO ROOT WORD >>>
def lemmatization(dataset, tweetcolumn):
    """ Lemmatization is a more effective option than stemming because it converts the word into its root word,
    rather than just stripping the suffices.It makes use of the vocabulary and does a morphological analysis to
    obtain the root word. Therefore, we usually prefer using lemmatization over stemming. """
    dataset[tweetcolumn] = dataset[tweetcolumn].apply(lambda x: " ".join([Word(word).lemmatize() for word in x.split()]))
    return dataset
# <<< CONVERTING INTO ROOT WORD

# >>> CONVERTING WORDS INTO LOWER CASE >>>
def lower_case(dataset, tweetcolumn):
    """ Lower casing, This avoids having multiple copies of the same words.
     For example, while calculating the word count, ‘Analytics’ and ‘analytics’
     will be taken as different words."""
    dataset[tweetcolumn] = dataset[tweetcolumn].apply(lambda x: " ".join(x.lower() for x in x.split()))
    return dataset
# <<< CONVERTING WORDS INTO LOWER CASE <<<

# >>> CLASS CONSTRUCTOR TWEET_CLEANER >>>
class tweet_cleaner:
    """ This class helps in choosing specific parameters or running them all at once

        param dataset : dataFrame
            give it an DataFrame dataset
        param tweetcolumn:
            the column you want to preprocess
        param preprocessoptions=['noise','short_words','stop_words','rare_words','common_words','stemming','lemmatization','lower_case']
            if not defined all cleaning options run automatically else if you define them you do it in following way you can choose only between some of them too
        :return: return the dataset
    """
    global choiceList,dataset
    choiceList = {'noise': 'noise(dataset, tweetcolumn)',
                  'short_words': 'short_words(dataset, tweetcolumn)',
                  'stop_words': 'stop_words(dataset, tweetcolumn)',
                  'rare_words': 'rare_words(dataset, tweetcolumn)',
                  'common_words': 'common_words(dataset, tweetcolumn)',
                  'tokenization': 'tokenization(dataset, tweetcolumn)',
                  'stemming': 'stemming(dataset, tweetcolumn)',
                  'lemmatization': 'lemmatization(dataset, tweetcolumn)',
                  'lower_case': 'lower_case(dataset, tweetcolumn)'}
    def __new__(cls, dataset, tweetcolumn, preprocessoptions=False):

        cls.dataset = dataset
        cls.tweetcolumn= tweetcolumn
        cls.preprocessoptions = preprocessoptions
        if preprocessoptions:
            for option in cls.preprocessoptions:
                mycode = choiceList.get(option)
                cls.dataset = exec(mycode)
        else:
            for choice in choiceList:
                mycode= choiceList[choice]
                cls.dataset = exec(mycode)

        return dataset
# <<< CLASS CONSTRUCTOR TWEET_CLEANER <<<
